# calendar_scheduler

<details>
  <summary>Top screen of the app</summary>
<img src="https://gitlab.com/flutter_toys/calendar_scheduler/uploads/3e1481ddfad93c96dea1c7fb3aa1bf31/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_8.06.11.png" height ="50%" width ="50%"></img>
<img src="https://gitlab.com/flutter_toys/calendar_scheduler/uploads/9b35b80bc47bec72ae5d12ab480e8853/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-03-15_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_8.06.20.png" height ="50%" width ="50%"></img>
</details>

It is an application with the following characteristics.

> **SQLite** <br>
> **Drift(SQLite's ORM)**<br>
> **Table Calendar package**<br>
> **GETIT(Dependency injection)** <br>
> **Intl(multilingualization)**<br>


## Getting Started

currently using the package below, so please refer to it.

```
dependencies:
  table_calendar: ^3.0.3
  intl: ^0.17.0
  drift: ^1.4.0
  sqlite3_flutter_libs: ^0.5.0
  path_provider: ^2.0.0
  get_it: ^7.2.0

dev_dependencies:
  drift_dev: ^1.4.0
  build_runner: ^2.1.7

dependency_overrides:
  path: ^1.8.1
```

## cautions

1. When you style a table calendar, specifying a **border radius** results in an error. For the following reasons

```
  @override
  bool debugAssertIsValid() {
    assert(shape != BoxShape.circle || borderRadius == null); // Can't have a border radius if you're a circle.
    return super.debugAssertIsValid();
  }
```
so, it is recommended that you use box decoration to change the default style to a style other than a circle.
```
  outsideDecoration: BoxDecoration(
    shape: BoxShape.rectangle,
  ),
```

2. Steps Required for Applying **Intel Packages**

+ Add the language you want in a way that suits your widget.
+ In the main screen, be sure to add this package.
```
import 'package:intl/date_symbol_data_local.dart';
```
+ Finally, write the following code before runApp():
```
// Code exists before the runApp() so you must add this
WidgetsFlutterBinding.ensureInitialized();
// All languages are available, in the Intel package
await initializeDateFormatting();
```





